
mkdir -p build/python
cd build/python
swig -c++ -python -outcurrentdir ../../bindings/interface.i
python3 ../../bindings/python/setup.py install --user
cd ../../bindings/python
python3 example.py
