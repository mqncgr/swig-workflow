
%include "std_vector.i"
%include "std_string.i"
%include "stdint.i"

#define SOME_IMPORTANT_DEFINE

%module hello
%{
#include "../src/hello.h"
%}

namespace std {
	%template(vString) vector<string>;
	%template(vDouble) vector<double>;
	%template(vvDouble) vector<vector<double>>;
};

%include "../src/hello.h"
