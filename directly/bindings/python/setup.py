
from distutils.core import setup, Extension

hello_extension = Extension(
	'_hello',
	define_macros = [
		('MY_DEFINE', None),
	],
	sources = [
		'interface_wrap.cxx',
		'../../src/hello.cpp',
	],
	include_dirs = [
		'../../src',
	#	'/usr/include/eigen3/',
	],
	#extra_compile_args=['-std=c++2a'],
	#extra_link_args=['-lompl'],
)

setup(
	name = "hello",
	py_modules = ['hello'],
	ext_modules = [hello_extension]
)
