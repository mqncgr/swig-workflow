The idea is that your program is written in C++ inside the src folder and can be used as standalone C++ program but also provides bindings for other languages, which are all contained in the bindings folder.

In this project we create a static library and then link it both to the C++ and the python version. This way, python doesn't have to recompile your whole project over and over again.

We go through some hoops to make sure that all the files that are created during the build are contained in the build folder.

Be aware that for simplicity we do not use a virtual environment, this will actually install the example on your system. Check the console output to see where it copied which files or hustle with a virtual environment yourself.
