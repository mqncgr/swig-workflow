import sys
from distutils.core import setup, Extension

# https://stackoverflow.com/a/49139257
static_libraries = ['hullo_lib']
static_lib_dir = '../../build'
libraries = []
library_dirs = []

if sys.platform == 'win32':
    libraries.extend(static_libraries)
    library_dirs.append(static_lib_dir)
    extra_objects = []
else:  # POSIX
    extra_objects = [
        '{}/lib{}.a'.format(static_lib_dir, l) for l in static_libraries
    ]

hullo_extension = Extension('_hullo',
                            sources=['interface_wrap.cxx'],
                            include_dirs=['../../src'],
                            libraries=libraries,
                            library_dirs=library_dirs,
                            extra_objects=extra_objects)

setup(name="hullo", py_modules=['hullo'], ext_modules=[hullo_extension])
