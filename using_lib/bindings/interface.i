
%include "std_vector.i"
%include "std_string.i"
%include "stdint.i"

#define SOME_IMPORTANT_DEFINE

%module hullo
%{
#include "../src/hullo.h"
%}

namespace std {
	%template(vString) vector<string>;
	%template(vDouble) vector<double>;
	%template(vvDouble) vector<vector<double>>;
};

%include "../src/hullo.h"
