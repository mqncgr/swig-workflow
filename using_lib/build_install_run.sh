
mkdir -p build
cd build
cmake ..
echo "building library and c++ version..."
make
echo "running c++ version..."
./hullo
cd ..

mkdir -p build/python
cd build/python
echo "generating python bindings..."
swig -c++ -python -outcurrentdir ../../bindings/interface.i
echo "building and installing python bindings..."
python3 ../../bindings/python/setup.py install --user
echo "running python version..."
cd ../../bindings/python
python3 example.py
