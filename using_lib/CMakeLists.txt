
project(Hullo)
cmake_minimum_required(VERSION 3.16)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

include_directories("src")
include_directories("src/bullet/bullet")

add_library(hullo_lib
    src/hullo.cpp
)

add_executable(hullo
	src/main.cpp
)
target_link_libraries(hullo hullo_lib)